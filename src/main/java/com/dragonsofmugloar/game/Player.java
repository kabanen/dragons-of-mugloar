package com.dragonsofmugloar.game;

import com.dragonsofmugloar.dto.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class Player {
    private final ApiClient apiClient;
    private final List<Item> boughtItems;

    // game state variables
    private String gameId;
    private int lives;
    private int gold;
    private int level;
    private int score;
    private int turn;

    public Player() {
        this.apiClient = new ApiClient();
        this.boughtItems = new ArrayList<>();
    }

    public void play() {
        startNewGame();

        do {
            buy();
            List<Task> tasks = getTasks();
            List<Task> tasksToSolve = pickTasksToSolve(tasks);
            solve(tasksToSolve);
        } while(this.lives > 0);

        System.out.println();
        System.out.println("Game over!");
        System.out.println("Score: " + this.score);
        System.out.println("Dragon level: " + this.level);
        System.out.println("Turns played: " + this.turn);
        System.out.println("Game ID: " + this.gameId);
    }

    private void startNewGame() {
        StartGameResult startGameResult = this.apiClient.startNewGame();
        updateGameState(startGameResult);
    }

    private void updateGameState(StartGameResult startGameResult) {
        this.gameId = startGameResult.getGameId();
        this.lives = startGameResult.getLives();
        this.gold = startGameResult.getGold();
        this.level = startGameResult.getLevel();
        this.score = startGameResult.getScore();
        this.turn = startGameResult.getTurn();
    }

    private void updateGameState(ShoppingResult shoppingResult) {
        this.lives = shoppingResult.getLives();
        this.gold = shoppingResult.getGold();
        this.level = shoppingResult.getLevel();
        this.turn = shoppingResult.getTurn();
    }

    private void updateGameState(SolvingResult solvingResult) {
        this.lives = solvingResult.getLives();
        this.gold = solvingResult.getGold();
        this.score = solvingResult.getScore();
        this.turn = solvingResult.getTurn();
    }

    private void buy() {
        List<Item> items = this.apiClient.fetchItems();
        Collections.sort(items); // sort by price
        Item hpPot = items.get(0);

        // buy health potions up to 3 lives if possible
        while (this.lives < 3 && this.gold >= hpPot.getCost()) {
            System.out.println("Buying: " + hpPot);
            ShoppingResult shoppingResult = this.apiClient.buyItem(hpPot);
            updateGameState(shoppingResult);
        }

        // buy other items if possible
        for (int i = 1; i < items.size(); ++i) {
            Item item = items.get(i);
            if (item.getCost() > this.gold) {
                // if the item is too expensive then all of the following are too
                break;
            }
            if (this.boughtItems.contains(item)) {
                // don't buy same item twice
                continue;
            }
            System.out.println("Buying: " + item);
            ShoppingResult shoppingResult = this.apiClient.buyItem(item);
            updateGameState(shoppingResult);
            if (shoppingResult.isSuccess()) {
                this.boughtItems.add(item);
            }
        }
    }

    private List<Task> getTasks() {
        List<Task> tasks = this.apiClient.fetchTasks();
        tasks.stream().filter(Task::isEncrypted).forEach(this::decrypt);
        return tasks;
    }

    private void decrypt(Task task) {
        if (isRot13Encoded(task)) {
            decrypt(task, this::rot13decode);
        } else {
            decrypt(task, this::base64decode);
        }
    }

    private boolean isRot13Encoded(Task task) {
        return task.getId().contains(" ") || task.getMessage().contains(" ") || task.getProbability().contains(" ");
    }

    private void decrypt(Task task, Function<String, String> function) {
        task.setId(function.apply(task.getId()));
        task.setMessage(function.apply(task.getMessage()));
        task.setProbability(function.apply(task.getProbability()));
    }

    private String base64decode(String s) {
        return new String(Base64.getDecoder().decode(s), StandardCharsets.UTF_8);
    }

    private String rot13decode(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c >= 'a' && c <= 'm') {
                c += 13;
            } else if  (c >= 'A' && c <= 'M') {
                c += 13;
            } else if  (c >= 'n' && c <= 'z')  {
                c -= 13;
            } else if  (c >= 'N' && c <= 'Z') {
                c -= 13;
            }
            builder.append(c);
        }
        return builder.toString();
    }

    private List<Task> pickTasksToSolve(List<Task> tasks) {
        List<Task> tasksToSolve = new ArrayList<>();
        tasks.sort(new TaskComparator());
        int turn = 1;
        for (Task task : tasks) {
            if (task.getExpiresIn() < turn || isTrap(task)) {
                continue;
            }
            tasksToSolve.add(task);
            ++turn;
        }

        // this may happen if all the tasks are traps
        if (tasksToSolve.isEmpty()) {
            tasksToSolve.add(tasks.get(0));
        }
        return tasksToSolve;
    }

    private boolean isTrap(Task message) {
        return message.getMessage().startsWith("Steal") && !message.getMessage().endsWith("profits with the people.");
    }

    private void solve(List<Task> tasksToSolve) {
        for (Task task : tasksToSolve) {
            System.out.println("Solving: " + task);
            SolvingResult solvingResult = this.apiClient.solveTask(task);
            System.out.println("Result: " + solvingResult);
            updateGameState(solvingResult);
            if (this.lives < 2) {
                // don't risk with 1hp, go to shop
                break;
            }
        }
    }
}
