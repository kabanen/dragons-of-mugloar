package com.dragonsofmugloar.game;

import com.dragonsofmugloar.dto.Task;

import java.io.Serializable;
import java.util.Comparator;

public class TaskComparator implements Comparator<Task>, Serializable {
    @Override
    public int compare(Task task1, Task task2) {
        int points1 = calculatePoints(task1);
        int points2 = calculatePoints(task2);
        // sort by probability ratio first
        if (points1 > points2) {
            return -1;
        } else if (points2 > points1) {
            return 1;
        } else {
            // if equal sort by expiresIn
            if (task1.getExpiresIn() < task2.getExpiresIn()) {
                return -1;
            } else if(task2.getExpiresIn() < task1.getExpiresIn()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    private int calculatePoints(Task task) {
        return Probability.byValue(task.getProbability()).getRatio() * task.getReward();
    }
}
