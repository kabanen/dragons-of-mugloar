package com.dragonsofmugloar.game;

import com.dragonsofmugloar.dto.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class ApiClient {
    private static final String BASE_URL = "https://www.dragonsofmugloar.com/api/v2";
    private static final String NEW_GAME_URL = BASE_URL + "/game/start";
    private static final String TASKS_URL = BASE_URL + "/{gameId}/messages";
    private static final String SOLVE_URL = BASE_URL + "/{gameId}/solve/{taskId}";
    private static final String ITEMS_URL = BASE_URL + "/{gameId}/shop";
    private static final String BUY_URL = BASE_URL + "/{gameId}/shop/buy/{itemId}";

    private RestTemplate httpClient;
    private String gameId;

    public ApiClient() {
        this.httpClient = new RestTemplate();
    }

    public StartGameResult startNewGame() {
        StartGameResult startGame = this.httpClient.postForObject(NEW_GAME_URL, null, StartGameResult.class);
        if (startGame == null) {
            throw new IllegalArgumentException("Could not start new game");
        }
        this.gameId = startGame.getGameId();
        return startGame;
    }

    public List<Task> fetchTasks() {
        isGameStarted();
        Task[] tasks = this.httpClient.getForObject(TASKS_URL, Task[].class, this.gameId);
        if (tasks == null) {
            throw new IllegalStateException("Could not fetch tasks");
        }
        return Arrays.asList(tasks);
    }

    public SolvingResult solveTask(Task task) {
        isGameStarted();
        SolvingResult solvingResult = this.httpClient.postForObject(SOLVE_URL, null, SolvingResult.class, this.gameId, task.getId());
        if (solvingResult == null) {
            throw new IllegalStateException("Could not solve task");
        }
        return solvingResult;
    }

    public List<Item> fetchItems() {
        isGameStarted();
        Item[] items = this.httpClient.getForObject(ITEMS_URL, Item[].class, this.gameId);
        if (items == null) {
            throw new IllegalStateException("Could not fetch items");
        }
        return Arrays.asList(items);
    }

    public ShoppingResult buyItem(Item item) {
        isGameStarted();
        ShoppingResult shoppingResult = this.httpClient.postForObject(BUY_URL, null, ShoppingResult.class, this.gameId, item.getId());
        if (shoppingResult == null) {
            throw new IllegalStateException("Could not buy item");
        }
        return shoppingResult;
    }

    private void isGameStarted() {
        if (this.gameId == null) {
            throw new IllegalArgumentException("Game not started, use startNewGame() method");
        }
    }
}
