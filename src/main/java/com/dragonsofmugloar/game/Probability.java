package com.dragonsofmugloar.game;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Probability {
    IMPOSSIBLE("Impossible", 0),
    SUICIDE_MISSION("Suicide mission", 10),
    PLAYING_WITH_FIRE("Playing with fire", 20),
    RISKY("Risky", 30),
    RATHER_DETRIMENTAL("Rather detrimental", 40),
    GAMBLE("Gamble", 50),
    HMMM("Hmmm....", 60),
    QUITE_LIKELY("Quite likely", 70),
    WALK_IN_THE_PARK("Walk in the park", 80),
    PIECE_OF_CAKE("Piece of cake", 90),
    SURE_THING("Sure thing", 100);

    private final String value;
    private final int ratio;

    public static Probability byValue(String value) {
        for (Probability probability : values()) {
            if (probability.getValue().equals(value)) {
                return probability;
            }
        }
        throw new IllegalArgumentException("Unknown probability " + value);
    }
}
