package com.dragonsofmugloar.dto;

import lombok.Data;

@Data
public class Item implements Comparable<Item> {
    private String id;
    private String name;
    private int cost;

    @Override
    public int compareTo(Item item) {
        if (this.cost < item.getCost()) {
            return -1;
        } else if (this.cost > item.getCost()) {
            return 1;
        } else {
            return 0;
        }
    }
}
