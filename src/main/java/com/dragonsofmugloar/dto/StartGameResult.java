package com.dragonsofmugloar.dto;

import lombok.Data;

@Data
public class StartGameResult {
    private String gameId;
    private int lives;
    private int gold;
    private int level;
    private int score;
    private int highScore;
    private int turn;
}
