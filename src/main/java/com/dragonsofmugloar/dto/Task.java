package com.dragonsofmugloar.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Task {
    @JsonProperty("adId")
    private String id;
    private String message;
    private int reward;
    private int expiresIn;
    private String probability;
    private boolean encrypted;
}
