package com.dragonsofmugloar.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ShoppingResult {
    @JsonProperty("shoppingSuccess")
    private boolean success;
    private int gold;
    private int lives;
    private int level;
    private int turn;
}
