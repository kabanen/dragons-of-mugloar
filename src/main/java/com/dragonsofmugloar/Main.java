package com.dragonsofmugloar;

import com.dragonsofmugloar.game.Player;

public class Main {
    public static void main(String[] args) {
        Player player = new Player();
        player.play();
    }
}
