# Dragons of Mugloar

This repository contains source code of the solution for the scripting adventure.
See https://www.dragonsofmugloar.com/

## Getting started

Follow these instructions to build and run the application.

### Prerequisites

My solution requires only Java JDK version 8.

To check, run:

```
$ java -version
openjdk version "1.8.0_181"
```

### Building and running

You can run the application by using Gradle `run` task. To do so run Gradle Wrapper script as follows:

```
$ ./gradlew run
```

this will run the game and print log to the standard output.

It is also possible to build executable Java Archive, to build run Gradle `clean build` task:

```
$ ./gradlew clean build
```

this results in `dragons-of-mugloar.jar` file in the `build/libs` directory.

To run the built archive:

```
$ java -jar build/libs/dragons-of-mugloar.jar
``` 

